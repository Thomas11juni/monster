<?php


function dd ($thing)
{
    echo '<pre>';

    var_dump($thing);

    echo '</pre>';

    die;
}


// Styles and scripts
// ---------------------------------------- -->

function get_asset_uri ($type, $filename)
{
    return get_template_directory_uri() . '/assets/' . $type . '/' . $filename;
}

function monster_scripts ()
{
    
    wp_enqueue_style('pinyon', 'https://fonts.googleapis.com/css?family=Pinyon+Script');

    wp_enqueue_style('lato', 'https://fonts.googleapis.com/css?family=Lato:400,700');

    wp_enqueue_style('playfair', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700');

    wp_enqueue_style('bootstrap', get_asset_uri('css', 'bootstrap.min.css'));

    wp_enqueue_style('style', get_stylesheet_uri());

    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');

    wp_enqueue_script('bootstrap', get_asset_uri('js', 'bootstrap.min.js'));
}

add_action('wp_enqueue_scripts', 'monster_scripts');

function remove_scripts_styles_version($src)
{
    if (strpos($src, 'ver=')) $src = remove_query_arg('ver', $src);

    return $src;
}

add_filter('style_loader_src', 'remove_scripts_styles_version', 9999);

add_filter('script_loader_src', 'remove_scripts_styles_version', 9999);


// Customizable theme sections
// ---------------------------------------- -->

function monster_section_home ($wp_customize)
{
    monster_customize_button_register();

    $wp_customize->add_section('monster_section_home', [
        'title' => 'Home'
    ]);


    $wp_customize->add_setting('monster_section_home_title', [
        'default' => 'Dining with love'
    ]);

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'monster_section_home_title_control', [
        'label' => 'Title',
        'section' => 'monster_section_home',
        'settings' => 'monster_section_home_title'
    ]));

    $wp_customize->add_setting('monster_section_home_paragraph', [
        'default' => '"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."'
    ]);

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'monster_section_home_paragraph_control', [
        'label' => 'Paragraph',
        'section' => 'monster_section_home',
        'settings' => 'monster_section_home_paragraph',
        'type' => 'textarea'
    ]));

    $wp_customize->add_setting('monster_section_home_background');

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'monster_section_home_background_control', [
        'label' => 'Background',
        'section' => 'monster_section_home',
        'settings' => 'monster_section_home_background'
    ]));

    $wp_customize->add_setting('monster_section_home_button_left_text', [
        'default' => 'text'
    ]);

    $wp_customize->add_setting('monster_section_home_button_left_url', [
        'default' => 'url'
    ]);

    $wp_customize->add_control(new Monster_Customize_Button_Control($wp_customize, 'monster_section_home_button_left_control', [
        'label' => 'Left button',
        'section' => 'monster_section_home',
        'settings' => [
            'button_text' => 'monster_section_home_button_left_text',
            'button_url' => 'monster_section_home_button_left_url'
        ]
    ]));
}

add_action('customize_register', 'monster_section_home');

function monster_section_about ($wp_customize)
{
    monster_customize_button_register();

    $wp_customize->add_section('monster_section_about', [
        'title' => 'About'
    ]);


    $wp_customize->add_setting('monster_section_about_pre_title', [
        'default' => 'About our'
    ]);

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'monster_section_about_pre_title_control', [
        'label' => 'Pre-title',
        'section' => 'monster_section_about',
        'settings' => 'monster_section_about_pre_title'
    ]));

    $wp_customize->add_setting('monster_section_about_title', [
        'default' => 'Chef'
    ]);

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'monster_section_about_title_control', [
        'label' => 'Title',
        'section' => 'monster_section_about',
        'settings' => 'monster_section_about_title'
    ]));

    $wp_customize->add_setting('monster_section_about_paragraph', [
        'default' => 'Fusce volutpat viverra blala tortor viverra. Morbi felis magna, varius ac faucibus eget, viverra a leo. Quisque at turpis amet diam ultricies euismod ac in dolor. In a lorem velit. Lorem ipsum dolor sit amet. Consectetur adipiscing elit.'
    ]);

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'monster_section_about_paragraph_control', [
        'label' => 'Paragraph',
        'section' => 'monster_section_about',
        'settings' => 'monster_section_about_paragraph',
        'type' => 'textarea'
    ]));

    $wp_customize->add_setting('monster_section_about_chef_image');

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'monster_section_about_chef_image_control', [
        'label' => 'Chef Image',
        'section' => 'monster_section_about',
        'settings' => 'monster_section_about_chef_image',
        'height' => 170,
        'width' => 170
    ]));

    $wp_customize->add_setting('monster_section_about_button_text', [
        'default' => 'Read more',
    ]);

    $wp_customize->add_setting('monster_section_about_button_url', [
        'default' => '#'
    ]);

    $wp_customize->add_control(new Monster_Customize_Button_Control($wp_customize, 'monster_section_about_button_control', [
        'section' => 'monster_section_about',
        'settings' => [
            'button_text' => 'monster_section_about_button_text',
            'button_url' => 'monster_section_about_button_url'
        ]
    ]));

    $wp_customize->add_setting('monster_section_about_right_image');

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'monster_section_about_right_image_control', [
        'label' => 'Right Image',
        'section' => 'monster_section_about',
        'settings' => 'monster_section_about_right_image',
        'height' => 560,
        'width' => 560
    ]));
}

add_action('customize_register', 'monster_section_about');


// Custom customize controllers
// ---------------------------------------- -->

function monster_customize_button_register ()
{
    if (class_exists('Monster_Customize_Button_Control')) return;

    class Monster_Customize_Button_Control extends WP_Customize_Control
    {

        public $label = 'Button';

        public function render_content ()
        {
            ?>
                <span class="customize-control-title">
                    <?php echo $this->label; ?>
                </span>

                <label>
                    Text
                    <input type="text" value="<?php echo $this->value('button_text'); ?>" <?php $this->link('button_text'); ?>>
                </label>

                <label>
                    Url
                    <input type="text" value="<?php echo $this->value('button_url'); ?>" <?php $this->link('button_url'); ?>>
                </label>
            <?php
        }
    }
}

add_action('customize_register', 'monster_customize_button_register');

// ---------------------------------------- -->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Monster wordpress template</title>

        <?php wp_head(); ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <section id="home" class="bg-centered disp-t text-center" style="background-image: url(<?php echo get_theme_mod('monster_section_home_background') ?: 'http://via.placeholder.com/1920x1000'; ?>);">
            <div class="disp-tc vert-middle">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="title title-pinyon">
                                <?php echo get_theme_mod('monster_section_home_title'); ?>
                            </h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <p>
                                <?php echo get_theme_mod('monster_section_home_paragraph'); ?>
                            </p>
                        </div>
                    </div>

                    <div class="row buttons">
                        <div class="col-lg-2 col-lg-offset-4 col-md-3 col-md-offset-3 col-sm-6">
                            <a href="<?php echo get_theme_mod('monster_section_home_button_left_url'); ?>" class="btn btn-block btn-secondary">
                                <?php echo get_theme_mod('monster_section_home_button_left_text'); ?>
                            </a>
                        </div>

                        <div class="col-lg-2 col-md-3 col-sm-6">
                            <a href="#" class="btn btn-block btn-primary">
                                Book a table
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="left-col col-sm-6 text-center">
                        <h2 class="title-pinyon">
                            <span class="pre-title black">
                                <?php echo get_theme_mod('monster_section_about_pre_title'); ?>
                            </span>

                            <?php echo get_theme_mod('monster_section_about_title'); ?>
                        </h2>

                        <p>
                            <?php echo get_theme_mod('monster_section_about_paragraph'); ?>
                        </p>

                        <?php
                            $src = 'http://via.placeholder.com/170x170';

                            $chef_image = get_theme_mod('monster_section_about_chef_image');

                            if ($chef_image)
                            {
                                $attach_src = wp_get_attachment_image_src($chef_image);

                                if (count($attach_src))
                                {
                                    $src = $attach_src[0];
                                }
                            }
                        ?>

                        <img class="img-circle" src="<?php echo $src; ?>">

                        <h4 class="color-primary">
                            Name Surname

                            <small>Lorem ipsum</small>
                        </h4>

                        <a href="<?php echo get_theme_mod('monster_section_about_button_url'); ?>" class="btn btn-secondary">
                            <?php echo get_theme_mod('monster_section_about_button_text'); ?>
                        </a>    
                    </div>

                    <div class="right-col col-sm-6">
                        <?php
                            $src = 'http://via.placeholder.com/560x560';

                            $right_image = get_theme_mod('monster_section_about_right_image');

                            if ($right_image)
                            {
                                $attach_src = wp_get_attachment_image_src($right_image);

                                if (count($attach_src))
                                {
                                    $src = $attach_src[0];
                                }
                            }
                        ?>

                        <img class="img-responsive img-circle" src="<?php echo $src; ?>">
                    </div>
                </div>
            </div>
        </section>

        <section id="specialities" class="bg-centered text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="menu-card">
                            <h2 class="title-pinyon">
                                <span class="pre-title white">Discover our</span>

                                Specialities
                            </h2>

                            <p class="intro playfair uppercase">
                                Best dishes recommended by our regular customers.
                            </p>

                            <ul class="menu-list">
                                <li class="menu-list-item">
                                    <h4 class="title">Lorem ipsum dolor sit amet</h4>

                                    <p class="description">
                                        Ut rhoncus velit justo, id luctus dolor consectetur at.
                                    </p>

                                    <p class="price">
                                        $11.50
                                    </p>
                                </li>
                                
                                <li class="menu-list-item">
                                    <h4 class="title">Lorem ipsum dolor sit amet</h4>

                                    <p class="description">
                                        Ut rhoncus velit justo, id luctus dolor consectetur at.
                                    </p>

                                    <p class="price">
                                        $11.50
                                    </p>
                                </li>

                                <li class="menu-list-item">
                                    <h4 class="title">Lorem ipsum dolor sit amet</h4>

                                    <p class="description">
                                        Ut rhoncus velit justo, id luctus dolor consectetur at.
                                    </p>

                                    <p class="price">
                                        $11.50
                                    </p>
                                </li>
                            </ul>

                            <div class="btn btn-primary">
                                Read more
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="services" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="service-col col-md-4">
                        <img src="http://via.placeholder.com/55x55">

                        <h4>Luxurious dining</h4>

                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euisuvfacilisis at vero eros et.
                        </p>
                    </div>

                    <div class="service-col col-md-4">
                        <img src="http://via.placeholder.com/55x55">

                        <h4>Delicious food</h4>

                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euisuvfacilisis at vero eros et.
                        </p>
                    </div>

                    <div class="service-col col-md-4">
                        <img src="http://via.placeholder.com/55x55">

                        <h4>Exquisite servicez</h4>

                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euisuvfacilisis at vero eros et.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section id="menu" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Entire Menu</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <h3>Lunch</h3>
                    </div>

                    <div class="col-sm-4">
                        <h3>Lunch</h3>

                        <div class="btn btn-primary">
                            Gallery
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <h3>Lunch</h3>
                    </div>
                </div>
            </div>
        </section>

        <section id="reservations">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <img class="img-responsive" src="http://via.placeholder.com/270x504">
                    </div>

                    <div class="col-md-3 hidden-sm hidden-xs">
                        <img class="img-responsive" src="http://via.placeholder.com/270x504">
                    </div>

                    <div class="col-md-6">
                        <div class="text-center">
                            <h2>Reserve a table!</h2>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed mi ac purus ultricies lacinia. Ut rhoncus velit justo, id dolor consectetur at vestibulum iaculis aliquet.
                            </p>

                            <p>
                                Nulla diam nunc, condimentum id ipsum id, pretium posuere. Quisque sollicitudin tempus augue non ultrices. 
                            </p>
                        </div>

                        <form class="row">
                            <div class="form-group col-sm-6">
                                <label>Name and Surname *</label>

                                <input type="text" class="form-control">
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Email *</label>

                                <input type="email" class="form-control">
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Date *</label>

                                <input type="date" class="form-control">
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Guest number *</label>

                                <input type="number" class="form-control">
                            </div>

                            <div class="text-center col-xs-12">
                                <input type="submit" value="Book now!" class="btn btn-default">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section id="reviews">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Guest reviews</h2>

                        <div id="reviews-carousel" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#reviews-carousel" data-slide-to="0" class="active"></li>

                                <li data-target="#reviews-carousel" data-slide-to="0"></li>

                                <li data-target="#reviews-carousel" data-slide-to="0"></li>
                            </ol>

                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-xs-1">
                                            "
                                        </div>

                                        <div class="col-xs-10">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed mi ac purus ultricies lacinia. Ut rhoncus velit justo, id luctus dolor consectetur at. Vestibulum laoreet elit vel iaculis aliquet. Nulla diam nunc, condimentum id ipsum.
                                            </p>

                                            <p>
                                                Quisque sollicitudin tempus augue non ultrices. Suspendisse dapibus metus in mi rutrum rhoncus. Duis et felis mattis, hendrerit orci eget, tristique risus. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                                            </p>

                                            <p>
                                                - George George, New York
                                            </p>
                                        </div>

                                        <div class="col-xs-1">
                                            "
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    b
                                </div>

                                <div class="item">
                                    c
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Get in touch</h2>
                    </div>

                    <div class="col-md-4">
                        <h4>Working hours</h4>

                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Monday - Thursday</strong>
                                
                                <br>
                                
                                8:00am - 11:00pm
                            </div>

                            <div class="col-xs-6">
                                <strong>Friday - Saturday</strong>

                                <br>

                                10:00am - 12:00pm
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <h4>Call/email us</h4>

                        <strong>Phone</strong> - 00386 10 868 100

                        <br>

                        <strong>Email</strong> - info@contact.com
                    </div>

                    <div class="col-md-5">
                        <h4>Location</h4>

                        <div class="row">
                            <div class="col-xs-6">
                                <strong>Moby Dick Street,</strong>
                                
                                <br>
                                
                                Nr. 258 NY USA
                            </div>

                            <div class="col-xs-6">
                                <strong>James Parkinson Street,</strong>

                                <br>

                                Nr. 11 TO Canada
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <div class="btn btn-default">
                    Full map
                </div>
            </div>
        </section>
    </body>
</html>
